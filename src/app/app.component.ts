import { Component, OnInit } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { Country } from './country';
import { CountrySearchService } from './country-search.service';
import { debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CountrySearchService]
})
export class AppComponent implements OnInit {
  
  countries: Observable<Country[]>;
  private searchTerms = new Subject<string>();
  private currentCountry:Country = {
    latlng: []
  };
  private hideSearchPage:boolean = false;
  private hideDetailPage:boolean = true;
  
  constructor(
    private countrySearchService: CountrySearchService
    ) {}
    
    search(term: string): void {
      // Push a search term into the observable stream.
      this.searchTerms.next(term);
    }
    
    ngOnInit(): void {
      this.countries = this.searchTerms.pipe(
        debounceTime(300), // wait for 300ms pause in events
        distinctUntilChanged(), // ignore if next search term is same as previous,
        switchMap(
          term =>
            term // switch to new observable each time
              ? // return the http search observable
                this.countrySearchService.search(term)
              : // or the observable of empty heroes if no search term
                of<Country[]>([])
        ),
        catchError(error => {
          // TODO: real error handling
          console.log(`Error in component ... ${error}`);
          this.ngOnInit();
          return of<Country[]>([]);
        })
      );
    }

    gotoDetail(country: Country): void {
      this.hideSearchPage = true;
      this.hideDetailPage = false;

      this.currentCountry = country;
    }

    goToList(): void {
      this.hideSearchPage = false;
      this.hideDetailPage = true;
    }
}
