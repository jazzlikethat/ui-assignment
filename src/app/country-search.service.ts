import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Country } from './country';

@Injectable({
  providedIn: 'root'
})
export class CountrySearchService {

  constructor(private http: HttpClient) { }

  search(term: string): Observable<Country[]> {
    return this.http
      .get<Country[]>(`https://restcountries.eu/rest/v2/name/${term}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return observableThrowError(res.error || 'Server error');
  }
}
